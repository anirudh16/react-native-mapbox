
# react-native-mapbox-navigation

## Getting started

`$ npm install react-native-mapbox-navigation --save`

### Mostly automatic installation

`$ react-native link react-native-mapbox-navigation`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-mapbox-navigation` and add `RNMapboxNavigation.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNMapboxNavigation.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNMapboxNavigationPackage;` to the imports at the top of the file
  - Add `new RNMapboxNavigationPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-mapbox-navigation'
  	project(':react-native-mapbox-navigation').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-mapbox-navigation/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-mapbox-navigation')
  	```

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNMapboxNavigation.sln` in `node_modules/react-native-mapbox-navigation/windows/RNMapboxNavigation.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using Mapbox.Navigation.RNMapboxNavigation;` to the usings at the top of the file
  - Add `new RNMapboxNavigationPackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNMapboxNavigation from 'react-native-mapbox-navigation';

// TODO: What to do with the module?
RNMapboxNavigation;
```
  