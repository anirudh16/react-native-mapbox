using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Mapbox.Navigation.RNMapboxNavigation
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNMapboxNavigationModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNMapboxNavigationModule"/>.
        /// </summary>
        internal RNMapboxNavigationModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNMapboxNavigation";
            }
        }
    }
}
